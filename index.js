const express = require('express');
const api = require('./consumers/ibge_service')

const server = express();

server.use(express.json());
server.listen(3000);
console.log('Server is running.');

server.get("/estados", async (req, res) => {
    try {
        const {data} = await api.get('/estados');

        return res.send(data);
    } catch (error) {
        res.send({error: error.message})
    }
});

server.get("/municipios/:uf", async (req, res) => {
   const {uf} = req.params;
    const {data} = await api.get(`/estados/${uf}/municipios`);

    return res.send(data);
});

server.get("/distritos/:codMunicipio", async (req, res) => {
    const {codMunicipio} = req.params;
    const {data} = await api.get(`/municipios/${codMunicipio}/distritos`);

    return res.send(data);
});
